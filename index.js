const express = require('express');
const app = express();
const multer = require('multer');
const path = require('path');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './images');
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, Date.now() + path.extname(file.originalname));
    }

});

const upload = multer({ storage });

app.use(express.json({ limit: '50mb' }));

app.set("view engine", "ejs");
app.get("/webcam", (req, res) => {
    res.render("webcam");
});

app.post("/webcam", upload.single('image'), (req, res) => {
    //req.body is the image
    const responsePayload = {
        message: 'webcam uploaded',
        file: req.file.filename
    };
    res.send(responsePayload);

});

app.listen(3001);
console.log("3001 is the port");